//
// Created by Richard Hodges on 09/09/2018.
//

#include <iostream>
#include <boost/asio.hpp>
#include <boost/system/system_error.hpp>

int main()
{
    auto exec = boost::asio::io_context();
    auto ec   = boost::system::error_code();
    exec.run(ec);
    std::cout << ec.message() << '\n';
}