#
# this file overrides hunter defaults.
# you can find hunter defaults here
# https://github.com/ruslo/hunter/blob/master/cmake/configs/default.cmake
#
hunter_config(Boost VERSION 1.68.0-p1 CMAKE_ARGS) # extra cmake args go after CMAKE_ARGS. e.g -DFOO=Bar
hunter_config(ZeroMQ VERSION 4.2.3-p1 CMAKE_ARGS "-DWITH_OPENPGM=1")
# ...etc... for each package you want to customise

# note that this is just a CMAKE script, so you can do if(APPLE) ... elseif(WINDOWS) ... endif() etc in this file
